# %%
"""
Project for ABEVT differential evolution alg implementation
Author: Tomas Holik

After run are evaluated all heuristic algorithms based on input params.
Statistical data are printed to console output and graphs are saved as images to /graphs directory

Author: Tomas Holik

"""

import numpy
import matplotlib.pyplot as plt
from TestFunctions import SchwefelTest
from DifferentialEvolutionAlg import DifferentialEvolutionAlg;


def showGraphs(title, iterationsCount, fitnessBestItList):
    fitnessBestItListNp = numpy.transpose(fitnessBestItList)
    print()
    print(title)
    print("Min:   %.5f" % numpy.min(fitnessBestItListNp))
    print("Max:   %.5f" % numpy.max(fitnessBestItListNp))
    print("Mean:  %.5f" % numpy.mean(fitnessBestItListNp))
    print("Median: %.5f" % numpy.median(fitnessBestItListNp))
    print("Std. dev.: %.5f" % numpy.std(fitnessBestItListNp))

    plt.plot(numpy.arange(1, iterationsCount + 1, dtype=int), getBestAverage(iterationsCount, fitnessBestItListNp))
    plt.title(title + " - Convergence graph of mean from best results")
    plt.xlabel("Generations")
    plt.ylabel("CF value")
    plt.savefig('graphs/' + title + '_graph_1.png', dpi=400)
    plt.show()

    plt.plot(numpy.arange(1, iterationsCount + 1, dtype=int), fitnessBestItListNp)
    plt.title(title + " - Convergence graph of best result of generation")
    plt.xlabel("Generations")
    plt.ylabel("CF value")
    plt.savefig('graphs/' + title + '_graph_2.png', dpi=400)
    plt.show()


def getBestAverage(iterationsCount, fitnessItList):
    fitnessBestAverage = []
    for i in range(iterationsCount):
        mean = numpy.mean(fitnessItList[i])
        fitnessBestAverage.append(mean)
    return fitnessBestAverage


def main():
    # Initialization
    totalTestFunctionCalls = 30
    dimensions=30
    iterationsCount = 150
    diffEvolutionSchwefel = DifferentialEvolutionAlg(totalTestFunctionCalls, SchwefelTest(), iterationsCount, dimensions)

    # # Evaluate all
    diffEvolutionSchwefel.process()

    # # Show results(statistical data to console output and graphs are saved as images)
    showGraphs("Differential evolution", iterationsCount,diffEvolutionSchwefel.fitnessBestItList)


if __name__ == '__main__':
    main()

import numpy
import math

"""
Project for ABEVT differential evolution alg implementation
Author: Tomas Holik

File contains implementation of all test functions

"""


class FirstDeJongTest:
    __min = -5
    __max = 5

    def evaluate(self, args):
        inner = numpy.power(args, 2)
        return numpy.sum(inner)

    def getMin(self):
        return self.__min

    def getMax(self):
        return self.__max


class SecondDeJongTest:
    __min = -5
    __max = 5

    def evaluate(self, args):
        x0 = args[0]
        x1 = args[1]
        return 100 * math.pow((x0 * x0 - x1), 2) + math.pow((1 - x0), 2)

    def getMin(self):
        return self.__min

    def getMax(self):
        return self.__max


class SchwefelTest:
    __min = -512
    __max = 512

    def evaluate(self, args):
        f = lambda x: -x * math.sin(math.sqrt(math.fabs(x)))
        inner = numpy.array([f(x) for x in args])
        return numpy.sum(inner)

    def getMin(self):
        return self.__min

    def getMax(self):
        return self.__max

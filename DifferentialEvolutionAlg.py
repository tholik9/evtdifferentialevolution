import numpy
import random
from random import sample
import math
"""
Project for ABEVT differential evolution alg implementation
Author: Tomas Holik

Class which contains implementation of DifferentialEvolution Rand/1/Bin
mutation type/number of differences in mutation/


"""


class DifferentialEvolutionAlg:

    def __init__(self, iterations, costFunction, iterationsCount, dimensions):
        self.iterations = iterations
        self.costFunction = costFunction
        self.iterationsCount = iterationsCount
        self.dimensions = dimensions
        self.F = 0.01
        self.CR = 0.15
        self.fitnessBestItList = []
        self.fitnessItList = []
        self.fitnessBest = None
        self.argsBest = None

    def process(self):
        random.seed(numpy.random.uniform(self.costFunction.getMin(), self.costFunction.getMax()))

        for i in range(self.iterations):
            self.processSearch()

    def processSearch(self):
        fitnessBestInOneIt = []

        # initialize population
        population = []
        populationCount = 50;
        for i in range(populationCount):
            population.append([self.__generateRandom() for n in range(self.dimensions)])

        for i in range(self.iterationsCount):
            populationNew = []
            for parentIndex in range(populationCount):
                # generate child
                child = self.__mutationBreedingSelection(population, parentIndex)

                fitnessParent = self.costFunction.evaluate(population[parentIndex])
                fitnessChild = self.costFunction.evaluate(child)

                if (fitnessChild < fitnessParent):
                    # child is in new population
                    populationNew.append(child)
                else:
                    # parent is in new population
                    populationNew.append(population[parentIndex])


            population = populationNew

            fitnessBestInOneIt.append(self.__getFitnessOfBest(population))

        self.fitnessBestItList.append(fitnessBestInOneIt)

    def __generateRandom(self):
        return numpy.random.uniform(self.costFunction.getMin(), self.costFunction.getMax())

    def __getFitnessOfBest(self, population):
        fitnessBest = 0
        for agent in population:
            # fitness = self.costFunction.evaluate(agent)
            fitness = self.costFunction.evaluate(agent)
            if fitnessBest is None or fitness < fitnessBest:
                fitnessBest = fitness
        return fitnessBest

    def __mutationBreedingSelection(self, population, activeAgentIndex):
        """rand/1 with binominal breeding
        u = x1 + F(x2 - x3)
        """
        populationCopy = population.copy()
        populationCopy.pop(activeAgentIndex)

        subset = sample(populationCopy, 3)
        mutationVector = numpy.asarray(subset[0]) + self.F * (numpy.asarray(subset[1]) - numpy.asarray(subset[2]))

        #binomial mutation
        result = []
        for i in range(self.dimensions):
            rand = numpy.random.uniform(0, 1)

            newx = mutationVector[i] if rand <= self.CR else population[activeAgentIndex][i]
            # 1) generate random if not in allowed range
            # if (newx < self.costFunction.getMin() or newx > self.costFunction.getMax()):
            #     result.append(self.__generateRandom())
            # else:
            #     result.append(newx)

            # 2) get min/max if not in allowed range
            # if (newx < self.costFunction.getMin()):
            #     result.append(self.costFunction.getMin())
            # elif (newx > self.costFunction.getMax()):
            #     result.append(self.costFunction.getMax())
            # else:
            #     result.append(newx)

            # 3) mirror if not in allowed range
            if newx < self.costFunction.getMin():
                newx = self.costFunction.getMin() + math.fabs(self.costFunction.getMin() - newx)
            elif newx > self.costFunction.getMax():
                newx = self.costFunction.getMax() - (newx - self.costFunction.getMax())
            result.append(newx)
        return result
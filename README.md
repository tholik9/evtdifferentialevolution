---
## Differential evolution algorithm

This project implement Differential evolution algorithm (DE) with binominal breeding and reflection border check.
As test function is used Schwefel test function.

After run are evaluated all heuristic algorithms based on input params.
Statistical data are printed to console output and graphs are saved as images to /graphs directory

Used python with anaconda plugin
